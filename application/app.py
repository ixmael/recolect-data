from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from flask_mail import Mail

import os

from models import db

app = Flask('save_data')

app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

# Email
mail_settings = {
    # "MAIL_SERVER": 'smtp.gmail.com',
    # "MAIL_PORT": 465,
    # "MAIL_USE_TLS": False,
    # "MAIL_USE_SSL": True,
    # "MAIL_USERNAME": os.environ['EMAIL_USER'],
    # "MAIL_PASSWORD": os.environ['EMAIL_PASSWORD']
    "MAIL_SERVER": 'localhost',
    "MAIL_PORT": 1025,
    # "MAIL_USE_TLS": False,
    # "MAIL_USE_SSL": True,
    # "MAIL_USERNAME": os.environ['EMAIL_USER'],
    # "MAIL_PASSWORD": os.environ['EMAIL_PASSWORD']
}

app.config.update(mail_settings)
mail = Mail(app)

# Base de datos
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data/data.sqlite'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.app = app
db.init_app(app)
db.create_all()

# Cargar las rutas y vistas
import views
app.register_blueprint(views.register)
app.register_blueprint(views.send)
app.register_blueprint(views.store)
