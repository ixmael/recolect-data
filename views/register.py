from flask import Blueprint, render_template

register = Blueprint('register', __name__)

@register.route('/registro', methods=['GET'])
def show_form():
    return render_template('index.html')
