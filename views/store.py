import os
import json
import uuid

from flask import Blueprint, render_template, request

store = Blueprint('store', __name__)

from models import db, FormData

@store.route('/registro', methods=['POST'])
def save_data():
    # Usar los datos del formulario
    data = request.form.to_dict(flat=True)

    # Guardar el archivo
    uploaded_file = request.files['idfile']
    if not uploaded_file.filename.lower().endswith('.pdf'):
        return render_template('error.html')

    uploaded_file_name = uuid.uuid4().hex
    uploaded_file.save('./data/files/{}.pdf'.format(uploaded_file_name))
    full_path = os.path.abspath('./data/files/{}.pdf'.format(uploaded_file_name))

    # Guardar la información
    form_data = FormData(data=json.dumps(data), file_id=uuid.uuid4().hex, path=full_path)
    db.session.add(form_data)
    db.session.commit()

    return render_template('stored.html')
