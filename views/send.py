import json
from flask import Blueprint, render_template, request
from flask_mail import Message

from application.app import mail
from models import FormData
from application.app import mail

send = Blueprint('sender', __name__)

@send.route('/enviar', methods=['GET'])
def send_data():
    forms = FormData.query.filter_by(reported=False).all()

    with mail.connect() as conn:
        for form in forms:
            data = json.loads(form.data)
            message = ['{}: {}\n'.format(k, data[k]) for k in data]
            msg = Message(
                recipients=mail,
                subject='Contacto registrado',
                body=message)

            with open(form.path, 'rb') as f:
                #   msg.attach("document.pdf", "image/png", f.read())
                msg.attach(
                    '{}.pdf'.format(form.file_id),
                    'application/octect-stream',
                    f.read())

                conn.send(msg)

    return render_template('send.html')
