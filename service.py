from dotenv import load_dotenv
load_dotenv()

from application import app

if __name__ == "__main__":
    app.run()
