# Recolectar datos
Este ejercicio sirve para mostrar la forma de recolectar datos con Python-Flask.

## Instalación
Con Python regularmente se usan [ambientes virtuales](http://docs.python.org.ar/tutorial/3/venv.html).

Una vez configurado el ambiente, las dependencias se instalan con:
```bash
pip install -r requeriments.txt
```

## Ejecutar servidor de prueba
Para iniciar el servidor de prueba se ejecuta:
```bash
python service.py
```
