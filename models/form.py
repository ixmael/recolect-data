from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class FormData(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.Text, nullable=False)
    file_id = db.Column(db.String(128), unique=True, nullable=False)
    path = db.Column(db.String(1024), unique=True, nullable=False)
    reported = db.Column(db.Boolean, default=False)
